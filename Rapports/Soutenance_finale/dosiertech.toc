\select@language {french}
\contentsline {chapter}{\numberline {1}Remerciement}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Introduction}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Sujet de notre soutenance}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Organisation}{4}{section.2.2}
\contentsline {chapter}{\numberline {3}Architecture}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Architecture actuelle}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Architecture finale}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}Virtualisation}{11}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Principe de la Virtualisation}{11}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}M\IeC {\'e}thodes de virtualisation}{12}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}L'isolation}{12}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}Hypervision}{12}{subsubsection.3.3.2.2}
\contentsline {subsection}{\numberline {3.3.3}KVM}{13}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}QEMU}{13}{subsubsection.3.3.3.1}
\contentsline {subsection}{\numberline {3.3.4}Haute disponibilit\IeC {\'e}}{14}{subsection.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.4.1}PRA}{14}{subsubsection.3.3.4.1}
\contentsline {section}{\numberline {3.4}Stockage}{15}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Principe du Stockage}{15}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Diff\IeC {\'e}rentes solutions}{16}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}GlusterFS}{16}{subsubsection.3.4.2.1}
\contentsline {subsubsection}{\numberline {3.4.2.2}NFS}{17}{subsubsection.3.4.2.2}
\contentsline {subsubsection}{\numberline {3.4.2.3}BeeGFS}{18}{subsubsection.3.4.2.3}
\contentsline {subsubsection}{\numberline {3.4.2.4}Comparatif}{19}{subsubsection.3.4.2.4}
\contentsline {section}{\numberline {3.5}Clustering}{20}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Principe du Clustering}{20}{subsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.1.1}Quorum}{20}{subsubsection.3.5.1.1}
\contentsline {subsection}{\numberline {3.5.2}Diff\IeC {\'e}rentes solutions}{20}{subsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.2.1}Proxmox VE}{20}{subsubsection.3.5.2.1}
\contentsline {subsubsection}{\numberline {3.5.2.2}Ganeti}{20}{subsubsection.3.5.2.2}
\contentsline {subsubsection}{\numberline {3.5.2.3}oVirt}{20}{subsubsection.3.5.2.3}
\contentsline {subsubsection}{\numberline {3.5.2.4}Comparatif}{21}{subsubsection.3.5.2.4}
\contentsline {section}{\numberline {3.6}R\IeC {\'e}seaux}{23}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Comparatif}{23}{subsection.3.6.1}
\contentsline {section}{\numberline {3.7}Monitoring}{24}{section.3.7}
\contentsline {chapter}{\numberline {4}Outils utilis\IeC {\'e}s}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Planner}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}VPN}{25}{section.4.2}
\contentsline {section}{\numberline {4.3}X2GO}{25}{section.4.3}
\contentsline {section}{\numberline {4.4}Vagrant}{25}{section.4.4}
\contentsline {section}{\numberline {4.5}Git}{26}{section.4.5}
\contentsline {section}{\numberline {4.6}LaTeX}{26}{section.4.6}
\contentsline {section}{\numberline {4.7}Google Drive}{26}{section.4.7}
\contentsline {section}{\numberline {4.8}Draw.io}{26}{section.4.8}
\contentsline {chapter}{\numberline {5}Probl\IeC {\`e}mes rencontr\IeC {\'e}s}{27}{chapter.5}
\contentsline {chapter}{\numberline {6}Conclusion}{28}{chapter.6}
\contentsline {chapter}{\numberline {7}Bibliographie}{29}{chapter.7}
\contentsline {chapter}{\numberline {8}Annexes}{31}{chapter.8}
